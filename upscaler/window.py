# window.py: main window
#
# Copyright (C) 2022 Hari Rana / TheEvilSkeleton
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-only


import os
import subprocess
import re
import time
from os.path import basename
from gi.repository import Adw, Gtk, GLib, Gdk, Gio
from sys import exit
from upscaler.threading import RunAsync
from upscaler.file_chooser import FileChooser

@Gtk.Template(resource_path='/io/gitlab/theevilskeleton/Upscaler/gtk/window.ui')
class UpscalerWindow(Adw.ApplicationWindow):
    __gtype_name__ = 'UpscalerWindow'

    """ Declare child widgets. """
    toast = Gtk.Template.Child()
    stack_upscaler = Gtk.Template.Child()
    button_input = Gtk.Template.Child()
    action_image_size = Gtk.Template.Child()
    action_upscale_image_size = Gtk.Template.Child()
    button_upscale = Gtk.Template.Child()
    spinner_loading = Gtk.Template.Child()
    image = Gtk.Template.Child()
    # video = Gtk.Template.Child()
    combo_models = Gtk.Template.Child()
    string_models = Gtk.Template.Child()
    # spin_scale = Gtk.Template.Child()
    button_output = Gtk.Template.Child()
    label_output = Gtk.Template.Child()
    button_cancel = Gtk.Template.Child()
    progressbar = Gtk.Template.Child()

    """ Initialize function. """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        """ Declare default models. """
        self.model_images = {
            'realesrgan-x4plus': _('Photo'),
            'realesrgan-x4plus-anime': _('Cartoon/Anime'),
        }

        """ Display models. """
        for model in self.model_images.values():
            self.string_models.append(model)

        """ Connect signals. """
        self.button_input.connect('clicked', self.__open_file)
        self.button_upscale.connect('clicked', self.__upscale)
        self.button_output.connect('clicked', self.__output_location)
        self.combo_models.connect('notify::selected', self.__set_model)
        self.button_cancel.connect('clicked', self.__cancel)
        # self.spin_scale.connect('value-changed', self.__update_post_upscale_image_size)

        # self.model_videos = [
        #     'realesr-animevideov3',
        # ]

    """ Open file and display it if the user selected it. """
    def __open_file(self, *args):
        FileChooser.open_file(self)

    """ Select output file location. """
    def __output_location(self, *args):
        FileChooser.output_file(self)

    """ Update progress. """
    def __upscale_progress(self, progress):
        if self.stack_upscaler.get_visible_child_name() == 'stack_upscaling':
            self.set_progress(progress)

    def __upscale(self, *args):

        """ Since GTK is not thread safe, prepare some data in the main thread. """
        self.cancelled = False

        """ Appropriately close child windows. """
        def reset_widgets():
            self.button_upscale.set_sensitive(True)
            self.progressbar.set_text(_('Loading…'))
            self.progressbar.set_fraction(0)
            self.cancelled = False

        """ Run in a separate thread. """
        def run():
            command = ['realesrgan-ncnn-vulkan',
                       '-i', self.input_file_path,
                       '-o', self.output_file_path,
                       '-n', list(self.model_images)[self.combo_models.get_selected()],
                       '-s', '4',
                       ]
            self.process = subprocess.Popen(command, stderr=subprocess.PIPE, universal_newlines=True)
            print('Running: ', end='')
            print(*command)
            """ Read each line, query the percentage and update the progress bar. """
            for line in iter(self.process.stderr.readline, ''):
                print(line, end='')
                res = re.match('^(\d*.\d+)%$', line)
                if res:
                    GLib.idle_add(self.__upscale_progress, float(res.group(1)))

        """ Run after run() function finishes. """
        def callback(*args):
            if self.cancelled == True:
                self.toast.add_toast(Adw.Toast.new(_('Upscaling Cancelled')))
            else:
                self.upscaling_completed_dialog()

            self.stack_upscaler.set_visible_child_name('stack_upscale')
            reset_widgets()

        """ Run functions asynchronously. """
        RunAsync(run, callback)
        self.stack_upscaler.set_visible_child_name('stack_upscaling')
        self.button_upscale.set_sensitive(False)

    """ Ask the user if they want to open the file. """
    def upscaling_completed_dialog(self, *args):
        def response(_widget):
            path = f'file://{self.output_file_path}'
            Gtk.show_uri(self, path, Gdk.CURRENT_TIME)

        toast = Adw.Toast.new(_('Image upscaled'))
        toast.set_button_label(_('Open'))
        toast.connect('button-clicked', response)
        self.toast.add_toast(toast)

    """ Set model and print. """
    def __set_model(self, *args):
        print(_('Model name: {}').format(list(self.model_images)[self.combo_models.get_selected()]))

    """ Update post-upscale image size as the user adjusts the spinner. """
    # def __update_post_upscale_image_size(self, *args):
    #     upscale_image_size = [
    #         self.image_size[1] * int(self.spin_scale.get_value()),
    #         self.image_size[2] * int(self.spin_scale.get_value()),
    #     ]
    #     self.action_upscale_image_size.set_subtitle(f'{upscale_image_size[0]} × {upscale_image_size[1]}')

    """ Update progress. """
    def set_progress(self, progress):
        self.progressbar.set_text(str(progress) + " %")
        self.progressbar.set_fraction(progress / 100)

    """ Prompt the user to close the dialog. """
    def close_dialog(self, function):
        self.stop_upscaling_dialog = Adw.MessageDialog.new(
            self,
            _('Stop upscaling?'),
            _('You will lose all progress.'),
        )
        def response(dialog, response_id):
            if response_id == 'stop':
                function()

        self.stop_upscaling_dialog.add_response('cancel', _('_Cancel'))
        self.stop_upscaling_dialog.add_response('stop', _('_Stop'))
        self.stop_upscaling_dialog.set_response_appearance('stop', Adw.ResponseAppearance.DESTRUCTIVE)
        self.stop_upscaling_dialog.connect('response', response)
        self.stop_upscaling_dialog.present()

    """ Cancel dialog. """
    def __cancel(self, *args):
        def function():
            self.cancelled = True
            self.process.kill()
        self.close_dialog(function)

    """ Close dialog. """
    def do_close_request(self):
        if self.stack_upscaler.get_visible_child_name() == 'stack_upscaling':
            def function():
                exit()
            self.close_dialog(function)
            return True
